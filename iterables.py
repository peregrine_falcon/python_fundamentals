# # Power of two calculated using iterables

# class power_two:
#     # class attributes

#     # constructor
#     def __init__(self, maximum):
#         self.maximum = maximum
    
#     # iterator
#     def __iter__(self):
#         self.n = 0
#         return self
    
#     def __next__(self):
#         if self.n <= self.maximum:
#             result = 2 ** self.n
#             self.n += 1
#             return result
#         else:
#             raise StopIteration

# # using iter and next to iterate throught the objects
# a = power_two(10)
# i = iter(a)
# for _ in range(10):
#     print(next(i))

# # using for loop to iterate through the object
# for i in power_two(10):
#     print(i)

# infinite iterables
class infinite_even_numbers:
    # class attributes

    # constructor
    
    # iterator
    def __iter__(self):
        self.n = 2
        return self
    
    def __next__(self):
        result = self.n + 2
        self.n += 1
        return result

a = infinite_even_numbers()
i = iter(a)
for _ in range(10):
    print(next(i))
        