# # Basic class
# class Parrot:
#     # class attributes
#     species = 'bird'

#     def __init__(self, name, age):
#         #instance attributes
#         self.name = name
#         self.age  = age

# Parrot.species = 'Human'

# a = Parrot("a", 23)
# print(a.name, a.age, a.__class__.species)

# b = Parrot("b", '29')
# print(b.name, b.age, b.species)

# # inheritance
# class Bird:
#     # class attributes
#     wings = 2

#     # constructor
#     def __init__(self, wingspan, body):
#         self.wingspan = wingspan
#         self.body = body

#     def suface_area(self):
#         return self.wingspan ** 2

#     def is_large(self):
#         return self.wingspan > 20

# class Penguin(Bird):
#     # constructor
#     def __init__(self, wingspan, body):
#         super().__init__(wingspan, body)

#     # method overriding
#     def is_large(self):
#         return self.body > 20

# a, b = Bird(10, 20), Penguin(20, 30)
# print(a.is_large(), b.is_large())

# # encapsulation
# # nothing can be truly private in python
# # adding __ just mangles the name of the method so that it does not interfere
# # with overridden methods and attributes
# # read for more info
# https://stackoverflow.com/questions/70528/why-are-pythons-private-methods-not-actually-private

# class Product:
#     # class attributes

#     # constructor
#     def __init__(self):
#         # pseudo private instance attribute
#         self.__price = 900

#     def selling_price(self):
#         return self.__price

#     # set method
#     def set_price(self, price):
#         self.__price = price


# a = Product()
# print(a.selling_price())
# a.__price = 1000
# print(a.selling_price())
# a.set_price(1000)
# print(a.selling_price())

# # why its is truly not private
# print(dir(a)) # returns list of all methods and attributes associated with
# the class of the object
# a._Product__price = 1300
# print(a.selling_price())

# polymorphism
# method over riding and overloading
# class bird():
#     # class attribute

#     # constructor

#     # method
#     def fly(self):
#         print('Bird can fly')
#
#     def swim(self):
#         print('Bird cannot swim')

# class penguin():
#     def fly(self):
#         print('Penguins cannot fly')

#     def swim(self):
#         print('Penguin can swim')

# def ability_test(obj):
#     obj.fly()
#     obj.swim()

# a, b  = bird(), penguin()

# ability_test(a)
# ability_test(b)
