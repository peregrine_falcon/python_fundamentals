# Decorator conditions, usage and sample examples

# Every thing in python is an objects. They can be assigned to other variables
def print_message(msg):
    print(msg)

first = print_message
# first("Hello")
second = first
# second("World")

# function can also be passed as arguments
# such functions are called higher order functions
def increment(n):
    return n + 1

def decrement(n):
    return n - 1

def perform_operation(func, n):
    return func(n)

# print(perform_operation(increment, 4), perform_operation(decrement, 4))

# functions can be called and returned
# decorates takes a function modifies the funtion and returns it

def make_pretty(func):
    def inner(msg):
        print("Pretty")
        func(msg)
    return inner

def ordinary(msg):
    print(msg)
# pretty = make_pretty(ordinary)
# pretty("Hello World")

# normally a function is decorated and reassigned to itself
# use @ + name of the decorator to modify

@make_pretty
def ordinary_1(msg):
    print(msg)
# print(ordinary_1("Hello Abhi"))



